import { Body, Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { UserCredentialDto } from './dto/user-credential.dto';
import { jwtPayload } from './jwt-payload.interface';
import { UserRepository } from './user.repository';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(UserRepository) 
        private userRepository: UserRepository,
        private jwtService: JwtService
    ){}

    registerUser(userCredentialDto: UserCredentialDto): Promise<void>{
        return this.userRepository.createUser(userCredentialDto)
    }

    async loginUser(userCredentialDto: UserCredentialDto): Promise<{ accessToken: string }>{
        const { username, password } = userCredentialDto

        const user = await this.userRepository.findOne({ username })

        if(user && (await bcrypt.compare(password, user.password))){
            const payload: jwtPayload = { username }
            const accessToken: string = await this.jwtService.sign(payload)
            return { accessToken }
        }
        else{
            throw new UnauthorizedException('Please check your login credential!')
        }
    }
}
