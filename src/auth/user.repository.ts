import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import * as bcrypt from "bcrypt";
import { UserCredentialDto } from "./dto/user-credential.dto";
import { User } from "./user.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User>{
    async createUser(userCredentialDto: UserCredentialDto): Promise<void>{
        const { username, password } = userCredentialDto

        const salt = await bcrypt.genSalt()
        const hashPassword = await bcrypt.hash(password, salt)

        const user = this.create({ username, password: hashPassword })

        try{
            await this.save(user)
        }catch(error){
            if(error.code === '23505'){
                throw new ConflictException('Username is taken')
            }
            else{
                throw new InternalServerErrorException()
            }
        }
    }
}