import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserCredentialDto } from './dto/user-credential.dto';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService){}

    @Post('/register')
    register(@Body() userCredentialDto: UserCredentialDto){
        return this.authService.registerUser(userCredentialDto)
    }

    @Post('/login')
    login(@Body() userCredentialDto: UserCredentialDto): Promise<{ accessToken: string }>{
        return this.authService.loginUser(userCredentialDto)
    }
}
