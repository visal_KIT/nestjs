import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskStatus } from './task-status.enum';
import { TaskRepository } from './task.repository';
import { Task } from './task.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/auth/user.entity';

@Injectable()
export class TaskService {
    constructor(@InjectRepository(TaskRepository) private taskRepository: TaskRepository){}

    async getAllTasks(user: User): Promise<Task[]> {
        const tasks = await this.taskRepository.find({ where: { user } })
        return tasks
    }

    async getTaskById(id: string, user): Promise<Task> {
        const found = await this.taskRepository.findOne({where: { id, user}})

        if(!found){
            throw new NotFoundException()
        }
        return found
    }

    createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
        return this.taskRepository.createTask(createTaskDto, user)
    }

    async deleteTask(id: string, user: User): Promise<string> {
        const result = await this.taskRepository.delete({ id, user })
        if(result.affected === 0){
            throw new NotFoundException()
        }

        return "Successfully deleted!"
    }

    async updateTaskStatus(id: string, status: TaskStatus, user: User): Promise<Task>{
        const task = await this.getTaskById(id, user)
        
        if(!task){
            throw new NotFoundException()
        }

        task.status = status
        this.taskRepository.save(task)
        return task
    }
}
