import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskStatus } from './task-status.enum'
import { CreateTaskDto } from './dto/create-task.dto';
import { updateTaskDto } from './dto/update-task.dto';
import { Task } from './task.entity';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';

@Controller('task')
@UseGuards(AuthGuard())
export class TaskController {
    constructor(private TaskService:TaskService){}

    @Get()
    getAllTasks(@GetUser() user: User): Promise<Task[]> {
        return this.TaskService.getAllTasks(user)
    }

    @Get('/:id')
    getTaskById(@Param('id') id:string, @GetUser() user: User): Promise<Task>{
        return this.TaskService.getTaskById(id, user)
    }

    @Post()
    createTask(@Body() createTaskDto: CreateTaskDto, @GetUser() user: User): Promise<Task> {
        return this.TaskService.createTask(createTaskDto, user)
    }

    @Delete('/:id')
    deleteTask(@Param('id') id:string, @GetUser() user: User): Promise<string>{
        return this.TaskService.deleteTask(id, user)
    }

    @Patch('/:id')
    updateTaskStatus(@Param('id') id:string, @Body() updateTaskDto: updateTaskDto, @GetUser() user: User): Promise<Task>{
        const { status } = updateTaskDto
        return this.TaskService.updateTaskStatus(id, status, user)
    }
}
